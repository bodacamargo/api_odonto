const db =  require('./db')
const Sequelize = require('sequelize')

  const Project = db.sequelize.define('projects', {
    id: { //nome do projeto
      type: Sequelize.INTEGER,
      required: true,
      primaryKey: true
    },
    projectName: { //nome do projeto
      type: Sequelize.STRING,
      required: true
    },
    coord: { //nome do coordenador do projeto
      type: Sequelize.STRING,
      required: true
    },
    subCoord: { //nome do subcoordenador do projeto
      type: Sequelize.STRING
    },
    siex:{ //numero do registro siex
      type: Sequelize.INTEGER,
      required: true
    },
    startAt:{ //data de inicio do projeto
      type: Sequelize.STRING,
      required: true
    },
    startSubs:{ //inicio das inscrições
      type: Sequelize.STRING,
      required: true
    },
    endSubs:{ //fim das inscrições
      type: Sequelize.STRING,
      required: true
    },
    description:{ //resumo do projeto
      type: Sequelize.TEXT,
      required: true
    },
    schoolHist:{ //historico escolar necessario?
      type: Sequelize.BOOLEAN
    },
    studentEffic:{ //aproveitamento escolar necessario?
      type: Sequelize.BOOLEAN
    },
    outro:{ //detalhes adicionais
      type: Sequelize.STRING
    },
    location:{ //Local de Funcionamento
      type: Sequelize.STRING,
      required: true
    },
    date:{ //Dia e horário de funcionamento
      type: Sequelize.STRING,
      required: true
    },
    vagas:{ //Dia e horário de funcionamento
      type: Sequelize.INTEGER,
    },
    pbext:{ //Dia e horário de funcionamento
      type: Sequelize.BOOLEAN,
    },
    afirmativas:{ //Dia e horário de funcionamento
      type: Sequelize.BOOLEAN,
    },
    type:{ //Dia e horário de funcionamento
      type: Sequelize.STRING,
    },
    status:{ //Dia e horário de funcionamento
      type: Sequelize.STRING,
    },
  })
  

export default Project;