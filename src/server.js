import express from 'express'
import morgan from 'morgan'
import session from 'express-session'
import bodyParser from 'body-parser'
import Project from './models/Project'
//const Project = require('../src/models/Project')(sequelize, DataTypes);
import cors from 'cors'

//iniciando server
const app = express()

//configs
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors())

//rotas
/* app.get('/api/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});
app.post('/api/world', (req, res) => {
  console.log(req.body);
  res.send(
    `I received your POST request. This is what you sent me: ${req.body.post}`,
  );
}); */

/* app.get('/', (req, res) => {
  if (!req.session.userName && !req.session.visitCount) {
    req.session.userName = 'user'
    req.session.visitCount = 1
    res.status(201).send(req.session)
  } else {
    if (req.session.userName === "user" && req.session.password === req.session.password)
      req.session.visitCount += 1
    res.status(200).send(req.session)
  }
}) */

app.get('/projetos', (req, res) => {
  Project.findAll().then((projetos) => {
    res.send(projetos);
  })
})

app.post('/projetos/criar', (req, res) => {
  Project.create(req.body).then(() => {
    res.redirect('/')
  }).catch((error) => {
    res.send('Erro ao criar projeto' + error)
  })
})

app.get('/projetos/deletar/:id', (req, res) => {
  Project.destroy({ where: { 'id': req.params.id } })
  res.redirect('/projetos')
})

app.put(`/projetos/editar/:id`, function (req, res, next) {
  delete req.body.editar
  try {

    Project.update(
      req.body, /* set attributes' value */
      { where: { id: req.body.id } } /* where criteria */
    )

    res.redirect('/')

  } catch (error) {
    console.log('deu erro', error)
  }

})

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));
  // Handle React routing, return all requests to React app
  app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

const port = 3001;

app.listen(port, () => {
  console.log('listen ', port)
})
